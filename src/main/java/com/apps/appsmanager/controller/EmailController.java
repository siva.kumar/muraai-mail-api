/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apps.appsmanager.controller;

import com.apps.appsmanager.response.Status;
import com.apps.appsmanager.services.EmailValidator;
import com.apps.appsmanager.services.MailService;
import com.apps.appsmanager.services.MailRequest;
import com.apps.appsmanager.services.MailRequestWithInvoice;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MY PC
 */
@RestController
@RequestMapping("/v/service")
public class EmailController {
 
    @Autowired
    private MailService mailService;
    
    @Autowired
    private EmailValidator validator;
    
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    @ResponseBody
    public String pingPong()
    {
        return "Started";
        
    }
    
    @RequestMapping(value = "/sendinvoice", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Status> sendEmailNotification(@RequestBody MailRequestWithInvoice request){
        
        Status status = new Status();
        if(request != null){
            if(validator.validate(request.getEmail())){
                mailService.sendEmailNotification(request.getEmail(),request.getSubject(), request.getInvoiceNumber(), request.getContent());
                status.setMessage("sucess mail has sent");
                status.setStatusCode(HttpStatus.OK);
                return new ResponseEntity<Status>(status, HttpStatus.OK);
            }
            else{
                status.setMessage("Email Fomatt Is Worng");
                status.setStatusCode(HttpStatus.BAD_REQUEST);
                return new ResponseEntity<Status>(status, HttpStatus.BAD_REQUEST);
            }
        }
         
        status.setMessage("No Content Present");
        status.setStatusCode(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<Status>(status, HttpStatus.BAD_REQUEST);
    }
    
    @RequestMapping(value = "/send", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Status> sendEmail(@RequestBody MailRequest request){
        
        Status status = new Status();
        if(request != null){
            if(validator.validate(request.getEmail())){
                mailService.sendEmailNotification(request.getEmail(),request.getSubject(), request.getContent());
                status.setMessage("sucess mail has sent");
                status.setStatusCode(HttpStatus.OK);
                return new ResponseEntity<Status>(status, HttpStatus.OK);
            }
            else{
                status.setMessage("Email Fomatt Is Worng");
                status.setStatusCode(HttpStatus.BAD_REQUEST);
                return new ResponseEntity<Status>(status, HttpStatus.BAD_REQUEST);
            }
        }
         
        status.setMessage("No Content Present");
        status.setStatusCode(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<Status>(status, HttpStatus.BAD_REQUEST);
    }
}
