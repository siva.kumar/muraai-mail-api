/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apps.appsmanager.services;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 *
 * @author admin
 */
@Service("mailService")
public class MailService {

    @Autowired
    private MailSender mailSender;

    @Autowired
    private JavaMailSender mailSenderRich;
    
    public void sendEmailNotification(String to, String subject, String invoiceNumber, String body) {
    
        final MimeMessage mimeMessage = mailSenderRich.createMimeMessage();
        final MimeMessageHelper helper;
        
        try {
            
            mimeMessage.setContent("<h1>sending html mail check</h1>", "text/html");
            helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            helper.setTo(to);
            helper.setSubject(this.composeEmail(subject, invoiceNumber));
            String html = "";
            String htmlText = "<html><body>"+this.composeEmail(body, invoiceNumber)+"</body></html>";
            helper.setText(htmlText, "html");
            mimeMessage.setContent(htmlText, "text/html");
            mailSenderRich.send(mimeMessage);
            
        } catch (MessagingException ex) {
            Logger.getLogger(MailService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
    public void sendEmailNotification(String to, String subject, String body) {
    
        final MimeMessage mimeMessage = mailSenderRich.createMimeMessage();
        final MimeMessageHelper helper;
        
        try {
            
            mimeMessage.setContent("<h1>sending html mail check</h1>", "text/html");
            helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            helper.setTo(to);
            helper.setSubject(subject);
            String html = "";
            String htmlText = "<html><body>"+body+"</body></html>";
            helper.setText(htmlText, "html");
            mimeMessage.setContent(htmlText, "text/html");
            mailSenderRich.send(mimeMessage);
            
        } catch (MessagingException ex) {
            Logger.getLogger(MailService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public String composeEmail(String data, String invoiceNumber){
  
        String actulaData = data;
        String withInvoiceNumber = actulaData.replace("${invoice_no}", invoiceNumber);
        return withInvoiceNumber;
    }
    

}

