/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apps.appsmanager.services;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author admin
 */

@Getter
@Setter
public class MailRequestWithInvoice {
 
    private String email;
    private String subject;
    private String invoiceNumber;
    private String content;
    
}
